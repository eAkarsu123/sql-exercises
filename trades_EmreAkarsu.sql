--1. Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. 
--This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.

select * from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'
order by size desc;

--2. Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, 
--minus the sum of the product of size and price for all buys.


--3. Develop a view that shows profit or loss for all traders.

-- trader takes a position on trade, can only open a position by making a trade.



