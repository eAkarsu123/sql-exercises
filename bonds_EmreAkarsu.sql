--Q1
select * from bond where CUSIP = '28717RH95';

--Q2
select * from bond order by maturity;

--Q3
select sum(quantity*price) as '3.Portfolio Value' from bond;

--Q4
select quantity*(coupon/100) as '4. Annual Return' from bond;

--Q5
select * from bond b
join rating r
on b.rating = r.rating
where ordinal <='3'
order by ordinal;

--Q6
select rating, avg(price) as 'price', avg(coupon) as 'coupon' 
from bond group by rating

--Q7
select bond.CUSIP, rating.rating from bond join rating on rating.rating = bond.rating
where bond.coupon/bond.price < rating.expected_yield